
```{r}
library(EBImage)
library(tidyverse)
library(ggrepel)
library(magrittr)
library(zoo)

#rmarkdown::render('dme383_plate_validation_analysis.Rmd',output_dir=getwd(),knit_root_dir=getwd())
```


```{r,readin,cache=T}

image_files <- 
    list.files(path="../../../data/dme383_construct_and_test_ppis_clonally_mdhfr/final-plate-scans/",
        pattern=".*_mtx_.*tif",full.names=T)
image_files

tester_col_lookup <- setNames(
            unlist(lapply(
                c('CALM3','ORMDL1','KDELR2',
                        'TAS2R7','TAS2R41',
                        'NCF2','ABHD16A','RPL30',
                        'RABL2A','ANXA1','DGCR8'
                    ),
                rep,2))
            ,1:22)
tester_col_lookup
tester_row_lookup <- setNames(
        unlist(lapply(
            c('NSP2','NSP4','ORF7B','ORF6','ORF7A',
                            'N','Sfrag1'),
            rep,2))
        ,1:14)
tester_row_lookup
lookup <- bind_rows(
        bind_cols(
            expand.grid(horf=tester_col_lookup,vorf=tester_row_lookup) ,
            expand.grid(col=as.numeric(names(tester_col_lookup)),
                row=as.numeric(names(tester_row_lookup))) ,
            plate='testers'
            )
        ,
        bind_cols(
            expand.grid(horf=tester_col_lookup,vorf=rep('f12-f3',4)) ,
            expand.grid(col=as.numeric(names(tester_col_lookup)),row=c(1,2,7,8)) ,
            plate='controls'
            )
        ,
        bind_cols(
            expand.grid(horf=tester_col_lookup,vorf=rep('NULL-f3',4)) ,
            expand.grid(col=as.numeric(names(tester_col_lookup)),row=c(3,4,9,10)) ,
            plate='controls'
            )
        ,
        bind_cols(
            expand.grid(vorf=c(tester_row_lookup,"NSP14","NSP14"),
                    horf=rep('f12-NULL',4)) ,
            expand.grid(col=c(as.numeric(names(tester_row_lookup)),15,16),
                    row=c(5,6,11,12)) ,
            plate='controls'
            )
        ,
        bind_cols(
            expand.grid(
                    vorf=c(rep('f12-f3',2),rep('NULL-f3',2)),
                    horf=rep('f12-NULL',4)) ,
            expand.grid(col=c(17:18,19:20),row=c(5,6,11,12)) ,
            plate='controls'
            )
        ) %>% tibble
lookup

diag_plots <- 1

datarz <- tibble(
        mtx=str_extract(image_files,pattern="_(\\d+)\\.tif",group=1) ,
        plate=str_extract(image_files,pattern="_d383_(.+)_mtx_",group=1) ,
        images=map(image_files,function(x) {
                img <- readImage(x)[,,1]
                colorMode(img) <- 0
                if (diag_plots) { display(img,"raster") }
                return(img)
            })
        )

```

```{r,function,cache=T}

registerPositions <- function(x,nrowz=14,ncolz=22,ann_color="red") {
# first take the rolling minimum of marginal means
    colsmooth <- apply(x,1,mean) %>% rollapply(100,min,partial=T)
    rowsmooth <- apply(x,2,mean) %>% rollapply(100,min,partial=T)
# then subtract those out
    backsubd <- apply(
            t(apply(x,1,function(x){x-rowsmooth}))
            ,2,function(x){x-colsmooth})
# renormalize to 0-1 range
    backsubd <- backsubd - min(backsubd)
    backsubd <- backsubd / max(backsubd)
    btbs <- thresh( gblur(backsubd,sigma=5) , w=100, h=100, offset=0.05) 
#display(btbs,'raster')
# sum again on the margin
    colsumz <- apply(btbs,1,sum)
# filter out which columns have something substantial
    #colz_mtm <- which(colsumz>median(colsumz))
    colz_mtm <- which(colsumz>0)
# identify the slope of the sums on this margin
    col_slopez <- which( rollapply(
                #rollapply(
                colsumz[colz_mtm[1]:colz_mtm[length(colz_mtm)]]
                #    ,width=50,mean)
                , 20, function(y){ cor(1:length(y),y) })
            > 0 )
# use that to do a kmeans, starting where the slope is positive
    col_centers <- sort(as.integer(
                kmeans(colz_mtm,
                    #centers=seq(col_slopez[1],
                    #        col_slopez[length(col_slopez)],
                    centers=seq(colz_mtm[1]+50,
                            colz_mtm[length(colz_mtm)]-50,
                            length.out=ncolz)
                    )$centers))
# calc the sumz
    #rowsumz <- apply(backsubd,2,sum)
    rowsumz <- apply(btbs,2,sum)
# identify rows with something substatial
    #rowz_mtm <- which(rowsumz>median(rowsumz))
    rowz_mtm <- which(rowsumz>0)
# identify the slope of the sums on this margin
    row_slopez <- which( rollapply(
                #rollapply(
                rowsumz[rowz_mtm[1]:rowz_mtm[length(rowz_mtm)]]
                #    ,width=50,mean)
                , 20, function(y){ cor(1:length(y),y) }) 
            > 0 )
# use that to do a kmeans, starting where the slope is positive
    row_centers <- sort(as.integer(
                kmeans(which(rowsumz>median(rowsumz)),
                    centers=seq(rowz_mtm[1]+50,
                            rowz_mtm[length(rowz_mtm)]-50,
                    #centers=seq(row_slopez[1],
                    #        row_slopez[length(row_slopez)],
                            length.out=nrowz)
                    )$centers))
    if (diag_plots) { 
        par(mfcol=c(2,2))
        plot(colsumz,type='l',main='sums along columns')
        abline(v=col_centers,col=ann_color)
        plot(rowsumz,type='l',main='sums along rows')
        abline(v=row_centers,col=ann_color)
        backsubd %>% display(.,"raster")
        backsubd %>% display(.,"raster")
        abline(v=col_centers,col=ann_color,lwd=0.5)
        abline(h=row_centers,col=ann_color,lwd=0.5)
        points( expand.grid(col_centers,row_centers),col="red",pch=16,cex=0.7)
    }
    return(list(row_centers=row_centers,col_centers=col_centers))
}
#registerPositions(readImage("../../../data/dme383_construct_and_test_ppis_clonally_mdhfr/final-plate-scans/230506_d383_controls_mtx_100.tif")[,,1],nrowz=12)
#registerPositions(readImage("../../../data/dme383_construct_and_test_ppis_clonally_mdhfr/final-plate-scans/230506_d383_testers_mtx_100.tif")[,,1],nrowz=14)
#x <- readImage("../../../data/dme383_construct_and_test_ppis_clonally_mdhfr/final-plate-scans//230506_d383_testers_mtx_100.tif")[,,1]
#registerPositions(x,nrowz=14)

```

```{r,register,cache=T}

centers <- datarz %>%
    group_by(plate,mtx) %>%
    mutate(
        centers=pmap(list(images,plate,mtx),function(img,plt,mtx){
                    jpeg((paste0(Sys.Date(),"_register_",plate,"_",mtx,".jpeg")))
                    returnz <- registerPositions(img,
                        nrowz=ifelse(plt=='controls',12,14))
                    dev.off()
                    return(returnz)
                })
        ) %>%
    unnest_wider(centers) 

```

```{r,proc images to blur,cache=T}

centers_and_segmented <- centers %>%
    mutate(
        labeled=map(images,function(x){
                blurred <- gblur(x,sigma=5)
                threshed <- thresh(blurred, w=100, h=100, offset=0.05)
                eroded <- erode(threshed, makeBrush(11,shape="disc")) 
                dilated <- dilate(eroded, makeBrush(11,shape="disc")) 
                labeled <- bwlabel(dilated)
# NOTE not using the eroded/dilated stuff!!!
                labeled <- bwlabel(threshed)
                if (diag_plots) { 
                    jpeg((paste0(Sys.Date(),"_segment_",plate,"_",mtx,".jpeg")))
                    par(mfcol=c(3,2))
                    display(blurred,"raster") 
                    display(threshed,"raster") 
                    display(eroded,"raster") 
                    display(dilated,"raster") 
                    display(labeled,"raster") 
                    display(colorLabels(labeled),"raster") 
                    dev.off()
                }
                return(labeled)
            })
        ) 
```

```{r,colony size get,cache=T}

par(mfcol=c(1,1))
colony_sizes <- centers_and_segmented %>%
    mutate(
        results=pmap(list(labeled,row_centers,col_centers),
                function(x,rc,cc){
                    if (diag_plots) { 
                        display(colorLabels(x),"raster") 
                        points( expand.grid(cc,rc),col="red",pch=16)
                    }
                    resultz <- 
                        expand.grid(
                            row_center=rc,
                            col_center=cc
                            ) %>%
                        mutate(
                            label=unlist(pmap(list(col_center,row_center),function(z,y){
                                    lab <- as.numeric(x[z,y])
                                    if (length(lab) && lab!=0) { return(lab) }
                                    search_radius <- 20
                                    maybe_labs <- x[    seq(z-search_radius,
                                                            z+search_radius),
                                                        seq(y-search_radius,
                                                            y+search_radius)   ]
                                    maybe_labs <- maybe_labs[maybe_labs!=0]
                                    lab <- as.numeric(names(
                                            sort(table(maybe_labs),
                                                decreasing=T))[1])
                                    if (length(lab)) return(lab) else return(NA)
                                    }))
                            ) %>%
                        mutate(
                            colony_size=unlist(map(label,function(z){
                                        if(is.na(z)) { 
                                            return(0)
                                        } else { 
                                            return(sum(z==x)) 
                                        }
                                    }))
                            )
                    resultz
                })
        ) %>%
    select(-labeled,-images,-ends_with('centers')) %>%
    unnest(results) 

```



```{r,plotz,cache=T}

resultz <- colony_sizes %>% 
    group_by(mtx,plate) %>%
    mutate(
        which_row=as.numeric(factor(row_center,levels=sort(unique(row_center))))
        ,
        which_col=as.numeric(factor(col_center,levels=sort(unique(col_center))))
        ) %>%
    left_join(
        lookup %>% rename(which_row=row,which_col=col)
        ,by=c('which_row','which_col','plate')
        ) %>%
    group_by(which_row,which_col) %>% 
    mutate(ncs=colony_size/(colony_size[mtx==0])) 
resultz %>% ungroup() %>% sample_n(10)

resultz %>% 
    {
    ggplot(.)+theme_bw()+
    aes(x=mtx,y=colony_size,
        group=interaction(which_row,which_col),
        col=interaction(vorf,horf)
        )+
    guides(col="none")+
    facet_wrap(~plate)+
    geom_line()+
    geom_text_repel(data=filter(.,mtx==100,colony_size>500),
        max.overlaps=20,
        aes(label=interaction(vorf,horf))
        )+
    NULL
    }

resultz %>% 
    filter(plate=='controls') %>%
    {
    ggplot(.)+theme_bw()+
    aes(x=which_col,y=-which_row,size=colony_size,col=vorf)+
    geom_point()+
    facet_wrap(~mtx)+
    NULL
    }

resultz %>% 
    filter(plate=='testers') %>%
    {
    ggplot(.)+theme_bw()+
    aes(x=which_col,y=-which_row,size=colony_size,col=vorf)+
    geom_point()+
    facet_wrap(~mtx)+
    NULL
    }


resultz %>% 
    filter(mtx==100) %>%
    {
    ggplot(.)+theme_bw()+
    aes(x=ncs,y=colony_size)+
    geom_point()+
    facet_wrap(~plate)+
    geom_text_repel(
        data=filter(.,ncs>.5 | colony_size>800),
        aes(label=interaction(vorf,horf))
        )+
    xlab("normalized colony size")+
    NULL
    }


resultz %>% 
    filter(mtx==100) %>%
    {
    ggplot(.)+theme_bw()+
    aes(x=ncs)+
    guides(col="none")+
    facet_wrap(~plate)+
    geom_histogram(bins=100)+
    geom_point(
        data=filter(.,mtx==100,ncs>.5),
        y=0)+
    geom_text_repel(
        data=filter(.,mtx==100,ncs>.5),
        y=0,min.segment.length=0,max.overlaps=100,
        direction='y',
        aes(label=interaction(vorf,horf))
        )+
    NULL
    }

resultz %>% 
    filter(mtx==100) %>%
    filter(plate=='testers') %>%
    group_by(vorf,horf) %>%
    summarize(across(ncs,c(mean=mean,sd=sd))) %>%
    ungroup() %>% 
    arrange(-ncs_mean) %>%
    mutate(label=str_c(vorf,horf,sep="_")) %>%
    mutate(label=factor(label,levels=unique(label))) %>%
    {
    ggplot(.)+theme_bw()+
    aes(x=label,
        y=ncs_mean,ymin=ncs_mean-1.96*ncs_sd,ymax=ncs_mean+1.96*ncs_sd)+
    geom_errorbar()+
    theme(axis.text.x=element_text(angle=90))+
    NULL
    }


resultz %>% 
    filter(mtx==100) %>%
    filter(plate=='controls') %>%
    group_by(vorf,horf) %>%
    summarize(across(c(colony_size,ncs),c(mean=mean,sd=sd))) %>%
    ungroup() %>% 
    arrange(-ncs_mean) %>%
    mutate(label=str_c(vorf,horf,sep="_")) %>%
    mutate(label=factor(label,levels=unique(label))) %>%
    filter(!is.na(label)) %>%
    {
    ggplot(.)+theme_bw()+
    aes(x=label,
        col=grepl("f12-f3",label),
#        y=colony_size_mean,
#        ymin=colony_size_mean-1.96*colony_size_sd,
#        ymax=colony_size_mean+1.96*colony_size_sd)+
# seems like the same thing, but hey maybe ncs is better? because.... scaled?
        y=ncs_mean,
        ymin=ncs_mean-1.96*ncs_sd,
        ymax=ncs_mean+1.96*ncs_sd)+
    geom_errorbar()+
    theme(axis.text.x=element_text(angle=90))+
    NULL
    }


g <- resultz %>% 
    filter(mtx>0) %>%
    group_by(mtx,plate,vorf,horf) %>%
    summarize(across(c(colony_size,ncs),c(mean=mean,sd=sd))) %>%
    ungroup() %>% 
    arrange(-ncs_mean) %>%
    mutate(label=str_c(vorf,horf,sep="_")) %>%
    mutate(label=factor(label,levels=unique(label))) %>%
    {
    ggplot(.)+theme_bw()+
    facet_grid(mtx~plate,scales="free",space="free_x")+
    aes(x=label,
        y=ncs_mean,ymin=ncs_mean-1.96*ncs_sd,ymax=ncs_mean+1.96*ncs_sd)+
    ylab("Mean colony size,\ndivided by mean colony size with no MTX")+
    geom_point()+
    geom_errorbar()+
    theme(axis.text.x=element_text(angle=90))+
    NULL
    }
g


g <- resultz %>% 
    filter(mtx==100) %>%
    group_by(mtx,plate,vorf,horf) %>%
    summarize(across(c(colony_size,ncs),c(mean=mean,sd=sd))) %>%
    ungroup() %>% 
    arrange(-ncs_mean) %>%
    mutate(label=str_c(vorf,horf,sep="_")) %>%
    mutate(label=factor(label,levels=unique(label))) %>%
    {
    ggplot(.)+theme_bw()+
    facet_grid(.~plate,scales="free",space="free_x")+
    aes(x=label,
        #y=ncs_mean,ymin=ncs_mean-1.96*ncs_sd,ymax=ncs_mean+1.96*ncs_sd)+
        y=colony_size_mean,
        ymin=colony_size_mean-1.96*colony_size_sd,
        ymax=colony_size_mean+1.96*colony_size_sd)+
    ylab("Mean colony size (100ug/mL MTX)")+
    geom_point()+
    geom_errorbar()+
    theme(axis.text.x=element_text(angle=90))+
    NULL
    }
g

ggsave(paste0(Sys.Date(),"_dotandbar_100mtx_both.jpeg"),g,width=9,height=7)

basal_cv <- resultz %>% 
    filter(plate=='testers',mtx==0,ncs>0) %>%
    {sd(.$colony_size)/mean(.$colony_size)}
basal_cv

mean_colony_size_100 <- resultz %>% 
    filter(plate=='testers',mtx==100,colony_size>0) %>%
    {median(.$colony_size)}
mean_colony_size_100

mean_colony_size_100*basal_cv

g <- resultz %>% 
    filter(mtx==100) %>%
    group_by(mtx,plate,vorf,horf) %>%
    summarize(across(c(colony_size,ncs),c(mean=mean,sd=sd))) %>%
    ungroup() %>% 
    mutate(hit= colony_size_mean-1.96*colony_size_sd
                >= 
                (0+1.96*mean_colony_size_100*basal_cv)
        ) %>%
    ungroup() %>% 
    arrange(-colony_size_mean) %>%
    mutate(label=str_c(vorf,horf,sep="_")) %>%
    mutate(label=factor(label,levels=unique(label))) %>%
    {
    ggplot(.)+theme_bw()+
    facet_grid(.~plate+vorf,scales="free",space="free_x")+
    aes(x=label,
        #y=ncs_mean,ymin=ncs_mean-1.96*ncs_sd,ymax=ncs_mean+1.96*ncs_sd)+
        col=hit,
        y=colony_size_mean,
        ymin=colony_size_mean-1.96*colony_size_sd,
        ymax=colony_size_mean+1.96*colony_size_sd)+
    ylab("Mean colony size (100ug/mL MTX)")+
    geom_point()+
    geom_errorbar()+
    theme(axis.text.x=element_text(angle=90))+
    NULL
    }
g
ggsave(paste0(Sys.Date(),"_dotandbar_100mtx_hits_both_byvorf.jpeg"),g,width=9,height=7)

g <- resultz %>% 
    filter(mtx==100) %>%
    group_by(mtx,plate,vorf,horf) %>%
    summarize(across(c(colony_size,ncs),c(mean=mean,sd=sd))) %>%
    ungroup() %>% 
    mutate(hit= colony_size_mean-1.96*colony_size_sd
                >= 
                (0+1.96*mean_colony_size_100*basal_cv)
        ) %>%
    ungroup() %>% 
    arrange(-colony_size_mean) %>%
    mutate(label=str_c(vorf,horf,sep="_")) %>%
    mutate(label=factor(label,levels=unique(label))) %>%
    {
    ggplot(.)+theme_bw()+
    facet_grid(.~plate,scales="free",space="free_x")+
    aes(x=label,
        #y=ncs_mean,ymin=ncs_mean-1.96*ncs_sd,ymax=ncs_mean+1.96*ncs_sd)+
        col=hit,
        y=colony_size_mean,
        ymin=colony_size_mean-1.96*colony_size_sd,
        ymax=colony_size_mean+1.96*colony_size_sd)+
    ylab("Mean colony size (100ug/mL MTX)")+
    geom_point()+
    geom_errorbar()+
    theme(axis.text.x=element_text(angle=90))+
    NULL
    }
g
ggsave(paste0(Sys.Date(),"_dotandbar_100mtx_hits_both.jpeg"),g,width=9,height=7)


validation_results <- resultz %>% 
    filter(mtx==100) %>%
    group_by(mtx,plate,vorf,horf) %>%
    summarize(across(c(colony_size,ncs),c(mean=mean,sd=sd))) %>%
    ungroup() %>% 
    mutate(hit= colony_size_mean-1.96*colony_size_sd
                >= 
                (0+1.96*mean_colony_size_100*basal_cv)
        ) %>%
    ungroup() %>% 
    arrange(plate,vorf,horf) 
validation_results


validation_results %>% 
    write_csv(file='vhppiseq_plate_validation_results_230919.csv')

```


```{r,compare,cache=T}

callz <- read_csv('d359_ppis_stats_dump.csv')

valcal <- validation_results %>% 
    filter(plate=='testers')%>%
    select(
        genotype_forward=vorf,
        genotype_reverse=horf,
        colony_size_mean,
        colony_size_sd,
        validation_hit=hit
        ) %>%
    left_join(
        callz
        ,by=c('genotype_forward','genotype_reverse')
        ) %>%
    group_by( genotype_forward, genotype_reverse ,
        flag_forward,flag_reverse,
        colony_size_mean, validation_hit
        ) 
valcal

valcal %>%
    summarize(screen_hits=sum(ttest_fdr<0.05)) %>%
    filter(!is.na(validation_hit)) %>%
    ggplot()+theme_bw()+
    aes(x=screen_hits,fill=validation_hit)+
    geom_bar(position='stack')+
    NULL

g <- valcal %>%
    summarize(screen_hits=sum(ttest_fdr<0.05)) %>%
    ungroup() %>%
    filter(!is.na(validation_hit)) %>%
    group_by( genotype_forward, genotype_reverse ,
        colony_size_mean, validation_hit
        ) %>% 
    reframe(screen_hits=ifelse(is.na(unique(screen_hits)),0,max(screen_hits))) %>%
    distinct() %>%
    {
    ggplot(.)+theme_classic()+
    aes(x=screen_hits)+
    geom_bar(aes(fill=validation_hit),position='fill')+
    geom_label(
        data=group_by(.,screen_hits=screen_hits,validation_hit) %>% tally() %>%
                pivot_wider(names_from='validation_hit',
                    values_from='n',values_fill=0) %>%
                rowwise() %>%
                mutate(ypos=mean(c(0.5,`TRUE`/(`TRUE`+`FALSE`)),na.rm=T)),
        aes( label=paste0(`TRUE`,' / ',(`TRUE`+`FALSE`)), 
            y=ypos )
        )+
    ylab("Fraction validated by colony growth")+
    xlab('Replicates of PPIseq where PPI detected')+
    #guides(fill='none')+
    #scale_x_discrete(labels=c('No hit','FDR < 0.05 in at least one replicate'))+
    scale_fill_discrete('Validation?',
        labels=c(`FALSE`='No growth',`TRUE`='Strain grew'))+
    theme(legend.position="right")+
    #theme(axis.title.x=element_blank())+
    NULL
    }
g
ggsave(paste0(Sys.Date(),"_fracvalid_by_replicates.jpeg"),g,width=4,height=5)
ggsave(paste0(Sys.Date(),"_fracvalid_by_replicates.pdf"),g,width=4,height=5)

g <- valcal %>%
    summarize(screen_hits=sum(ttest_fdr<0.05)) %>%
    ungroup() %>%
    filter(!is.na(validation_hit)) %>%
    group_by( genotype_forward, genotype_reverse ,
        colony_size_mean, validation_hit
        ) %>% 
    reframe(screen_hits=ifelse(is.na(unique(screen_hits)),0,max(screen_hits))) %>%
    distinct() %>%
    {
    ggplot(.)+theme_classic()+
    aes(x=screen_hits>0)+
    geom_bar(aes(fill=validation_hit),position='fill')+
    geom_label(
        data=group_by(.,screen_hits=screen_hits>0,validation_hit) %>% tally() %>%
                pivot_wider(names_from='validation_hit',
                    values_from='n',values_fill=0) %>%
                rowwise() %>%
                mutate(ypos=mean(c(0.5,`TRUE`/(`TRUE`+`FALSE`)),na.rm=T)),
        aes( label=paste0(`TRUE`,' / ',(`TRUE`+`FALSE`)), 
            y=ypos )
        )+
    ylab("Fraction validated by colony growth")+
    #xlab('Replicates of PPIseq where PPI detected')+
    #guides(fill='none')+
    scale_x_discrete(labels=c('No PPI\ndetected in\npooled screen','PPIseq hit in\nat least one\nreplicate'))+
    scale_fill_discrete('Validation?',
        labels=c(`FALSE`='No growth',`TRUE`='Strain grew'))+
    theme(legend.position="right")+
    theme(axis.title.x=element_blank())+
    NULL
    }
g
ggsave(paste0(Sys.Date(),"_fracvalid_by_hitornot.jpeg"),g,width=4,height=5)
ggsave(paste0(Sys.Date(),"_fracvalid_by_hitornot.pdf"),g,width=4,height=5)

```




```{r}
# from https://stackoverflow.com/questions/36777390/r-list-all-packages-and-versions-used-in-a-markdown-file
installed.packages()[names(sessionInfo()$otherPkgs), "Version"]
```


